package com.samarcev.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "passenger_name")
    private String passengerName;

    @Column(name = "trip_id")
    private Integer tripId;

    @Column(name = "seat_no")
    private Integer seatNo;

    @Column(name = "cost")
    private Double cost;

    @Column(name = "user_id")
    private String userId;
}
