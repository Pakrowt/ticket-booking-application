package com.samarcev.service.implementation;

import com.samarcev.repository.implemantation.AdminRepository;
import com.samarcev.service.AdminServiceInterface;

public class AdminService implements AdminServiceInterface {
    public static final AdminService INSTANCE = new AdminService();
    AdminRepository adminRepository = new AdminRepository();

    private AdminService() {
    }

    @Override
    public boolean addBusTerminal(String city, String country) {
        return adminRepository.addBusTerminal(city, country);
    }

    @Override
    public boolean deleteBusTerminal(String city) {
        return adminRepository.deleteBusTerminal(city);
    }

    @Override
    public boolean modifyBusTerminal(String city, String country) {
        return adminRepository.modifyBusTerminal(city, country);
    }

    @Override
    public boolean addBus(String busNumber, String model, Integer totalSeats) {
        return adminRepository.addBus(busNumber, model, totalSeats);
    }

    @Override
    public boolean deleteBus(String busNumber) {
        return adminRepository.deleteBus(busNumber);
    }

    @Override
    public boolean modifyBus(String busNumber, String model, Integer totalSeats) {
        return adminRepository.modifyBus(busNumber, model, totalSeats);
    }

    public static AdminService getInstance() {
        return INSTANCE;
    }
}
