package com.samarcev.servlet;

import com.samarcev.entity.Role;
import com.samarcev.service.implementation.UserService;
import com.samarcev.util.RequestParam;
import com.samarcev.util.UrlPath;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet(UrlPath.LOGIN)
public class LoginValidateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserService userService = UserService.getInstance();
        String login = req.getParameter(RequestParam.LOGIN);
        String password = req.getParameter(RequestParam.PASSWORD);
        req.getSession().setAttribute(RequestParam.LOGIN, login);
        req.getSession().getServletContext().setAttribute(RequestParam.LOGIN, login);

        if (userService.checkUserByLoginAndPassword(login, password)) {
            if (userService.checkRole(login) == Role.USER) {
                log.info(" user {} is logged in", login);
                resp.sendRedirect("homepage.jsp");
            } else if (userService.checkRole(login) == Role.ADMIN) {
                resp.sendRedirect("adminpage.jsp");
                log.info(" admin {} is logged in", login);
            } else {
                log.error("UNKNOWN ROLE");
                resp.sendRedirect("unknown-error.html");
            }
        } else {
            resp.sendRedirect("registration.jsp");
            log.info("the login or password is incorrect");
        }

    }
}
