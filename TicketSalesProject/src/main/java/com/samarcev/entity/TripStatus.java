package com.samarcev.entity;

public enum TripStatus {
    ARRIVED,
    DEPARTED,
    CANCELLED,
    SCHEDULED
}
