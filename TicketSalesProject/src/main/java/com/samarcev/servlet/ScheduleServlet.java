package com.samarcev.servlet;

import com.samarcev.entity.Trip;
import com.samarcev.service.implementation.TripService;
import com.samarcev.util.UrlPath;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(UrlPath.SCHEDULE)
public class ScheduleServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Trip> allTrips = TripService.getInstance().getAllTrips();
        req.getServletContext().setAttribute("allTrips", allTrips);
        resp.sendRedirect("scheduled.jsp");
    }
}
