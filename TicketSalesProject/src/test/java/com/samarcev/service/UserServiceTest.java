package com.samarcev.service;

import com.samarcev.HibernateTestUtil;
import com.samarcev.entity.Role;
import com.samarcev.entity.User;
import com.samarcev.util.HibernateSessionFactoryUtil;
import lombok.Cleanup;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import static org.junit.jupiter.api.Assertions.*;


class UserServiceTest {

//    @Before
//    Session openSession() {
//        SessionFactory sessionFactory = HibernateTestUtil.getSessionFactory();
//        return sessionFactory.openSession();
//    }

    @Test
    void shouldCreateUser() {
        @Cleanup Session session = HibernateTestUtil.getSessionFactory().openSession();
        User user = new User();
        user.setLogin("testLogin");
        user.setName("testName");
        user.setPassword("testPassword");
        user.setRole(Role.USER);
        user.setMoney(1000.0);
        session.getTransaction().begin();
        session.getSession().save(user);
        session.getTransaction().commit();
        CriteriaQuery<User> criteriaQuery = session.getCriteriaBuilder().createQuery(User.class);
        criteriaQuery.select(criteriaQuery.from(User.class));
        assertEquals(1, session.createQuery(criteriaQuery).getResultList().size());


    }

    @Test
    void shouldDeleteUser() {
        @Cleanup Session session = HibernateTestUtil.getSessionFactory().openSession();
        User user = new User();
        user.setLogin("testLogin");
        user.setName("testName");
        user.setPassword("testPassword");
        user.setRole(Role.USER);
        user.setMoney(1000.0);
        session.getTransaction().begin();
        session.getSession().save(user);
        session.getTransaction().commit();

        assertEquals(user.getLogin(),session.get(User.class,user.getLogin()).getLogin(),"This method expected" +
                " user, if method throw NullPointerException, mean user not save in persistence context ");
        session.getTransaction().begin();
        session.delete(user);
        session.getTransaction().commit();
        assertNull(session.get(User.class, user.getLogin()));
    }

    @Test
    void shouldGetUserByLogin() {
    }

    @Test
    void shouldGetAllUser() {

    }

    @Test
    void shouldCheckUserByLoginAndPassword() {
    }

    @Test
    void shouldCheckRole() {
    }
}