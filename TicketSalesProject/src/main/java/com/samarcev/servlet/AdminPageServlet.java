package com.samarcev.servlet;


import com.samarcev.entity.User;
import com.samarcev.service.implementation.UserService;
import com.samarcev.util.RequestParam;
import com.samarcev.util.UrlPath;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(UrlPath.ADMIN_PAGE)
public class AdminPageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<User> allUsers = UserService.getInstance().getAllUser();
        req.getServletContext().setAttribute(RequestParam.ALL_USERS, allUsers);
        resp.sendRedirect("user-list.jsp");
    }
}
