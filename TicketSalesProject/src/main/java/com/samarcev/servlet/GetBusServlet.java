package com.samarcev.servlet;

import com.samarcev.service.implementation.BusService;
import com.samarcev.util.UrlPath;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(UrlPath.GET_BUS)
public class GetBusServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var busNumbers = BusService.getInstance().getBusNumbers();
        req.getServletContext().setAttribute("busNumbers", busNumbers);
        req.getRequestDispatcher("modify-bus.jsp").forward(req, resp);
    }
}
