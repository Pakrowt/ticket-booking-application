package com.samarcev.repository.implemantation;

import com.samarcev.entity.Bus;
import com.samarcev.entity.BusTerminal;
import com.samarcev.repository.AdminRepositoryInterface;
import com.samarcev.util.HibernateSessionFactoryUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;

@Slf4j
public class AdminRepository implements AdminRepositoryInterface {
    @Override
    public boolean addTrip() {
        return false;
    }

    @Override
    public boolean deleteTrip() {
        return false;
    }

    @Override
    public boolean modifyTrip() {
        return false;
    }

    @Override
    public boolean addBusTerminal(String city, String country) {
        try (Session session = HibernateSessionFactoryUtil.getSession();) {
            session.getTransaction().begin();
            BusTerminal busTerminal = BusTerminal.builder()
                    .city(city)
                    .country(country)
                    .build();
            session.save(busTerminal);
            log.info("successful create bus terminal - {}", busTerminal);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            log.error("bus terminal not created!");
            return false;
        }
    }

    @Override
    public boolean deleteBusTerminal(String city) {
        try (Session session = HibernateSessionFactoryUtil.getSession();) {
            session.getTransaction().begin();
            BusTerminal busTerminal = session.get(BusTerminal.class, city);
            log.info("getting bus terminal {}", busTerminal);
            session.delete(busTerminal);
            session.flush();
            log.info("successful delete bus terminal - {}", busTerminal);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            log.error("bus terminal not deleted!");
            return false;
        }
    }

    @Override
    public boolean modifyBusTerminal(String city, String country) {
        try (Session session = HibernateSessionFactoryUtil.getSession();) {
            session.getTransaction().begin();
            BusTerminal busTerminal = session.get(BusTerminal.class, city);
            log.info("getting bus terminal {}", busTerminal);
            busTerminal.setCountry(country);
            session.saveOrUpdate(busTerminal);
            log.info("successful modify bus terminal - {}", busTerminal.getCity());
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            log.error("bus terminal not modify!");
            return false;
        }
    }

    @Override
    public boolean addBus(String busNumber, String model, Integer totalSeats) {
        try (Session session = HibernateSessionFactoryUtil.getSession();) {
            session.getTransaction().begin();
            Bus bus = Bus.builder()
                    .id(busNumber)
                    .model(model)
                    .capacity(totalSeats)
                    .remainingSeats(totalSeats)
                    .build();
            session.save(bus);
            log.info("successful create bus - {}", bus);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            log.error("bus not created!");
            return false;
        }
    }

    @Override
    public boolean deleteBus(String busNumber) {
        try (Session session = HibernateSessionFactoryUtil.getSession();) {
            session.getTransaction().begin();
            Bus bus = session.get(Bus.class, busNumber);
            log.info("getting bus {}", bus);
            session.delete(bus);
            session.flush();
            log.info("successful delete bus - {}", bus);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            log.error("bus not deleted!");
            return false;
        }
    }


    @Override
    public boolean modifyBus(String busNumber, String model, Integer totalSeats) {
        try (Session session = HibernateSessionFactoryUtil.getSession();) {
            session.getTransaction().begin();
            Bus bus = session.get(Bus.class, busNumber);
            log.info("getting bus {}", bus);
            bus.setModel(model);
            bus.setCapacity(totalSeats);
            bus.setRemainingSeats(totalSeats);
            session.save(bus);
            log.info("successful modify bus - {}", bus);
            session.getTransaction().commit();
            return true;
        } catch (RuntimeException e) {
            log.error("bus not modify!");
            return false;
        }
    }
}