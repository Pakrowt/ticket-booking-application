package com.samarcev.entity;


import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@ToString(exclude = {"bus", "departureBusTerminal", "arrivalBusTerminal"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "departure_date")
    private LocalDateTime departureDate;

    @Column(name = "departure_location",insertable = false,updatable = false)
    private String departureLocation;

    @Column(name = "arrival_date")
    private LocalDateTime arrivalDate;

    @Column(name = "arrival_location",insertable = false,updatable = false)
    private String arrivalLocation;

    @Column(name = "bus_id",updatable = false,insertable = false)
    private String busId;

    @Enumerated(EnumType.STRING)
    private TripStatus status;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "bus_id")
    private Bus bus;

    @ManyToOne
    @JoinColumn(name = "departure_location")
    private BusTerminal departureBusTerminal;

    @ManyToOne
    @JoinColumn(name = "arrival_location")
    private BusTerminal arrivalBusTerminal;

}
