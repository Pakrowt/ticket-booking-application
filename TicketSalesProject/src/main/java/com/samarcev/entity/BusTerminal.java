package com.samarcev.entity;


import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@ToString(exclude = {"departureTripsList", "arrivalTripsList"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "bus_terminal")
public class BusTerminal {

    @Id
    private String city;

    private String country;

    @Builder.Default
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departureBusTerminal")
    private List<Trip> departureTripsList = new ArrayList<>();

    @Builder.Default
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "arrivalBusTerminal")
    private List<Trip> arrivalTripsList = new ArrayList<>();

    public void addDepartureTripToCity(Trip trip) {
        departureTripsList.add(trip);
        trip.setDepartureBusTerminal(this);
    }

    public void addArrivalTripToCity(Trip trip) {
        arrivalTripsList.add(trip);
        trip.setArrivalBusTerminal(this);
    }


}
