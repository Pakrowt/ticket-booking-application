package com.samarcev.util;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordCoder {

    public String codePass(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] bytes = md.digest(password.getBytes());
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02X", b));
        }
        return builder.toString();
    }

}
