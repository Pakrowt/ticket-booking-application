package com.samarcev.servlet;


import com.samarcev.service.implementation.UserService;
import com.samarcev.util.RequestParam;
import com.samarcev.util.UrlPath;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet(UrlPath.UPDATE_PASSWORD)
public class UpdatePasswordServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserService userService = UserService.getInstance();
        String oldPassword = req.getParameter("oldPassword");
        String newPassword = req.getParameter("newPassword");
        String confirmationPassword = req.getParameter("confirmationPassword");
        String login = (String) req.getSession().getAttribute(RequestParam.LOGIN);

        if (userService.getUser(login).getPassword().equals(oldPassword)) {
            if (userService.passwordMatching(newPassword, confirmationPassword)) {
                userService.updateUserPassword(login, newPassword);
                req.getSession().invalidate();
                resp.sendRedirect("login.jsp");
                log.info("Password {} successful update", login);
            } else {
                log.error("New password - {}  and confirmation password - {} do not match."
                        , newPassword, confirmationPassword);
                resp.sendRedirect("notmatchingpassword.html");
            }
        } else {
            log.error("Current password is invalid");
            resp.sendRedirect("currentpassworderror.html");
        }
    }
}
