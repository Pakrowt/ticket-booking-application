<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modify Bus</title>
</head>
<body>
<form action="adding-bus" method="POST">
    <h2>Изменение данных автобуса</h2>
    <p><b>Выберите автобус: </b> <br>
    <form method="post" action="get-bus">
        <select name="busNumbers">
            <c:forEach var="busNumber" items="${busNumbers}">
            <option value="<${busNumber}>" label="${busNumber}"><c:out value="${busNumber}"/><p></option>
            </c:forEach>
        </select>
    </form>
    <%--    <select name="busNumbers">--%>
    <%--        <option value="1">FIRST</option>--%>
    <%--        <option value="2">SECOND</option>--%>
    <%--    </select>--%>
    <p><b>Введите модель автобуса: </b><br>
        <input type="text" name="model" size="30"></p>
    <p><b>Введите кол-во мест в автобусе: </b><br>
        <input type="number" name="capacity" max="30"></p>
    <button type="submit">OK</button>
</form>
</body>
</html>
</body>
</html>
