package com.samarcev.service;

public interface AdminServiceInterface {

    //actions with bus terminal
    boolean addBusTerminal (String city, String country);
    boolean deleteBusTerminal (String city);
    boolean modifyBusTerminal (String city, String country);

    //actions with bus
    boolean addBus (String busNumber, String model, Integer totalSeats);
    boolean deleteBus (String busNumber);
    boolean modifyBus (String busNumber, String model, Integer totalSeats);
}
