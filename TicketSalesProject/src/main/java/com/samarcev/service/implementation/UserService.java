package com.samarcev.service.implementation;


import com.samarcev.entity.Role;
import com.samarcev.entity.User;
import com.samarcev.repository.implemantation.UserRepository;
import com.samarcev.service.UserServiceInterface;
import com.samarcev.util.HibernateSessionFactoryUtil;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

@Slf4j
public class UserService implements UserServiceInterface {

    UserRepository userRepository = new UserRepository();
    public static final UserService INSTANCE = new UserService();
    public static final String CHECK_USER_BY_LOGIN_AND_PASSWORD_HQL =
            "from User where login=: login and password =: password";
    public static final String CHECK_ROLE_HQL = "select role from User where login=: login";

    @Override
    public void createUser(String login, String password, String name, Role role, Double money) {
        userRepository.createUser(login, password, name, role, money);
    }

    @Override
    public void deleteUser(String login) {
        userRepository.deleteUser(login);
    }

    @Override
    public User getUser(String login) {
        return userRepository.getUser(login);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUser();
    }

    @Override
    public boolean passwordMatching(String firstPassword, String secondPassword) {
        if (!firstPassword.equals(secondPassword)) {
            log.error("The password and confirm password fields do not match");
            return false;
        } else return true;
    }

    @Override
    public void updateUserPassword(String login, String password) {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction();
        User user = session.get(User.class, login);
        user.setPassword(password);
        session.update(user);
        session.getTransaction().commit();
    }

    @Override
    public boolean checkUserByLoginAndPassword(String login, String password) {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        Query<User> query = session.createQuery(CHECK_USER_BY_LOGIN_AND_PASSWORD_HQL, User.class);
        query.setParameter("login", login);
        query.setParameter("password", password);
        return !query.list().isEmpty();
    }

    @Override
    public Role checkRole(String login) {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        Query<Role> query = session.createQuery(CHECK_ROLE_HQL, Role.class);
        query.setParameter("login", login);
        return query.list().iterator().next();

    }

    private UserService() {
    }

    public static UserService getInstance() {
        return INSTANCE;
    }
}
