package com.samarcev.entity;

public enum Role {
    ADMIN,
    USER,
    UNKNOWN
}
