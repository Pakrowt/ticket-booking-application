package com.samarcev.service;

import com.samarcev.entity.Role;
import com.samarcev.entity.User;

import java.util.List;

public interface UserServiceInterface {

    void createUser(String login, String password, String name, Role role, Double money);

    void deleteUser(String login);

    User getUser(String login);

    List<User> getAllUser();

    boolean passwordMatching(String firstPassword, String secondPassword);

    void updateUserPassword(String login, String password);

    boolean checkUserByLoginAndPassword(String login, String password);

    Role checkRole(String login);


}
