<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>List of Users</h2></caption>
        <tr>
            <th>LOGIN</th>
            <th>ROLE</th>
            <th>NAME</th>
            <th>MONEY</th>
        </tr>
        <c:forEach var="user" items="${allUsers}">
            <tr>
                <td><c:out value="${user.login}"/></td>
                <td><c:out value="${user.role}"/></td>
                <td><c:out value="${user.name}"/></td>
                <td><c:out value="${user.money}"/></td>
                <td>
                <td>
                    <form method="POST" action='<c:url value="/deleteuser" />' style="display:inline;">
                        <input type="hidden" name="login" value="${user.login}">
                        <input type="submit" value="Delete">
                    </form>
                </td>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
