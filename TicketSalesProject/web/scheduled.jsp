<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SCHEDULE</title>
</head>
<body>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>Actual schedule</h2></caption>
        <tr>
            <th>Departure Location</th>
            <th>Departure Date</th>
            <th>Arrival Location</th>
            <th>Arrival Date</th>
            <th>Status</th>
        </tr>

        <c:forEach var="allTrips" items="${allTrips}">
            <tr>
                <td><c:out value="${allTrips.departureLocation}"/></td>
                <td><c:out value="${allTrips.departureDate}"/></td>
                <td><c:out value="${allTrips.arrivalLocation}"/></td>
                <td><c:out value="${allTrips.arrivalDate}"/></td>
                <td><c:out value="${allTrips.status}"/></td>
                <td>
                    <a href="edit?id=<c:out value='${user.login}' />">Buy</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>

