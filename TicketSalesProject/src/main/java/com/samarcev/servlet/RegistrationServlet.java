package com.samarcev.servlet;

import com.samarcev.entity.Role;
import com.samarcev.service.implementation.UserService;
import com.samarcev.util.PasswordCoder;
import com.samarcev.util.RequestParam;
import com.samarcev.util.UrlPath;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@WebServlet(UrlPath.REGISTRATION)
public class RegistrationServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter(RequestParam.LOGIN);
        String password = req.getParameter(RequestParam.PASSWORD);
        String name = req.getParameter(RequestParam.NAME);
        Role role = Role.USER;
        Double money = 0.0;

        PasswordCoder passwordCoder = new PasswordCoder();
        try {
            passwordCoder.codePass(password);
        } catch (NoSuchAlgorithmException e) {
            log.error("error hash-code", e);
        }
        UserService.getInstance().createUser(login, password, name, role, money);
        log.info("user {} was successfully created", login);
        resp.sendRedirect("login.jsp");
    }
}
