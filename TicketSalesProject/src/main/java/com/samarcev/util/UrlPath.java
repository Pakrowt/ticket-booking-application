package com.samarcev.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UrlPath {

    public static final String REGISTRATION = "/registration";
    public static final String LOGIN = "/validate";
    public static final String ADMIN_PAGE = "/admin";
    public static final String DELETE_USER = "/delete";
    public static final String LOGOUT = "/logout";
    public static final String ADMIN_DELETE_USER = "/deleteuser";
    public static final String SCHEDULE = "/schedule";
    public static final String UPDATE_PASSWORD = "/updatepass";
    public static final String ADDING_BUS = "/adding-bus";
    public static final String GET_BUS = "/get-bus";

}
