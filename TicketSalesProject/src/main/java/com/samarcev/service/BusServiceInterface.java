package com.samarcev.service;

import com.samarcev.entity.Bus;

import java.util.List;
import java.util.Optional;

public interface BusServiceInterface {

    Optional<Bus> getBusbyBusNumber(String busNumber);

    List<Bus> getAllBus();

    List<String> getBusNumbers ();
}
