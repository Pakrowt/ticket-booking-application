package com.samarcev.repository;

import com.samarcev.entity.Trip;

import java.util.List;

public interface TripRepositoryInterface {

    List<Trip> getAllTrips();
}
