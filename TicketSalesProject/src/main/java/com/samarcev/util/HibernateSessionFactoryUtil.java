package com.samarcev.util;

import com.samarcev.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

@Slf4j
public class HibernateSessionFactoryUtil {
    private HibernateSessionFactoryUtil() {
    }

    public static Session getSession() {
        Configuration configuration = buildConfiguration();
        configuration.configure();
        return configuration.buildSessionFactory().openSession();
    }

    public static Configuration buildConfiguration() {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(BusTerminal.class).addAnnotatedClass(Trip.class)
                .addAnnotatedClass(TripStatus.class).addAnnotatedClass(Bus.class)
                .addAnnotatedClass(Ticket.class).addAnnotatedClass(User.class);
        return configuration;
    }
}
