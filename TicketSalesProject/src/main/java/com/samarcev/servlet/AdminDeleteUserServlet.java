package com.samarcev.servlet;

import com.samarcev.service.implementation.UserService;
import com.samarcev.util.RequestParam;
import com.samarcev.util.UrlPath;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet(UrlPath.ADMIN_DELETE_USER)
public class AdminDeleteUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter(RequestParam.LOGIN);
        UserService.getInstance().deleteUser(login);
        log.info("delete user {}", login);
        resp.sendRedirect("user-list.jsp");
    }
}
