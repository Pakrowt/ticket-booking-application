package com.samarcev.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RequestParam {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String MONEY = "money";
    public static final String ROLE = "role";
    public static final String ALL_USERS = "allUsers";

}
