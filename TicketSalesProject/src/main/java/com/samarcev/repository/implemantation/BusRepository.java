package com.samarcev.repository.implemantation;

import com.samarcev.entity.Bus;
import com.samarcev.repository.BusRepositoryInterface;
import com.samarcev.util.HibernateSessionFactoryUtil;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

@Slf4j
public class BusRepository implements BusRepositoryInterface {
    @Override
    public Optional<Bus> getBusbyBusNumber(String busNumber) {
        Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction().begin();
        Bus bus = session.get(Bus.class, busNumber);
        session.beginTransaction().commit();
        return Optional.ofNullable(bus);
    }

    @Override
    public List<Bus> getAllBus() {
        Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction();
        List<Bus> busList = session.createQuery("from Bus ", Bus.class).setFirstResult(0).setMaxResults(10).getResultList();
        session.getTransaction().commit();
        return busList;
    }
}
