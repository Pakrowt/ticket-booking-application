package com.samarcev.servlet;

import com.samarcev.service.implementation.AdminService;
import com.samarcev.util.UrlPath;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(UrlPath.ADDING_BUS)
public class AddingBusServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        AdminService adminService = AdminService.getInstance();
        String busNumber = req.getParameter("busNumber");
        String model = req.getParameter("model");
        int capacity = Integer.parseInt(req.getParameter("capacity"));
        adminService.addBus(busNumber, model, capacity);
        resp.sendRedirect("adminpage.jsp");
    }
}
