<%--
  Created by IntelliJ IDEA.
  User: pavel
  Date: 02.02.2022
  Time: 21:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Добавление новых автобусов</title>
</head>
<body>
<form action="adding-bus" method="POST">
    <h2>Добавление автобуса</h2>
    <p><b>Введите регистрационный номер автобуса: </b> <br>
        <input type="text" name="busNumber" size="30"></p>
    <p><b>Введите модель автобуса: </b><br>
        <input type="text" name="model" size="30"></p>
    <p><b>Введите кол-во мест в автобусе: </b><br>
        <input type="number" name="capacity"max="30"> </p>
        <button type="submit">OK</button>
</body>
</html>
