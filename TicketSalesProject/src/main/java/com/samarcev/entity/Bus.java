package com.samarcev.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Bus {

    @Id
    @Column(name = "bus_number")
    private String id;

    private String model;

    @Column(name = "total_seats")
    private Integer capacity;

    @Column(name = "remaining_seats")
    private Integer remainingSeats;

    @OneToOne(mappedBy = "bus", cascade = CascadeType.ALL)
    private Trip trip;
}
