package com.samarcev.service;

import com.samarcev.entity.Trip;

import java.util.List;

public interface TripServiceInterface {

    List<Trip> getAllTrips();
}
