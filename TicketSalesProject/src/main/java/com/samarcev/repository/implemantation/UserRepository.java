package com.samarcev.repository.implemantation;

import com.samarcev.entity.Role;
import com.samarcev.entity.User;
import com.samarcev.repository.UserRepositoryInterface;
import com.samarcev.util.HibernateSessionFactoryUtil;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;

import java.util.List;

@Slf4j
public class UserRepository implements UserRepositoryInterface {

    @Override
    public void createUser(String login, String password, String name, Role role, Double money) {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction();

        User user = User.builder()
                .login(login)
                .password(password)
                .name(name)
                .role(role)
                .money(money)
                .build();
        session.save(user);
        session.getTransaction().commit();
    }

    @Override
    public void deleteUser(String login) {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction();
        User user = session.get(User.class, login);
        if (user != null) {
            session.delete(user);
            log.info("user is deleted");
            session.flush();
        }
        session.getTransaction().commit();
    }


    @Override
    public User getUser(String login) {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        session.setDefaultReadOnly(true);
        session.beginTransaction();
        User user = session.get(User.class, login);
        if (user == null) {
            log.error("not found user with login - {}", login);
        }
        return user;
    }

    @Override
    public List<User> getAllUser() {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction();
        List<User> userList = session.createQuery("from User", User.class).setFirstResult(0).setMaxResults(10).getResultList();
        session.getTransaction().commit();
        return userList;
    }
}
