package com.samarcev.repository;

import com.samarcev.entity.Bus;
import com.samarcev.entity.Trip;

import java.util.List;
import java.util.Optional;

public interface BusRepositoryInterface {

    Optional<Bus> getBusbyBusNumber(String busNumber);

    List<Bus> getAllBus();
}
