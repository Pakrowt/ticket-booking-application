package com.samarcev.service.implementation;

import com.samarcev.entity.Trip;
import com.samarcev.repository.implemantation.TripRepository;
import com.samarcev.service.TripServiceInterface;

import java.util.List;

public class TripService implements TripServiceInterface {
    TripRepository tripRepository = new TripRepository();
    public static final TripService INSTANCE = new TripService();

    @Override
    public List<Trip> getAllTrips() {
        return tripRepository.getAllTrips();
    }

    private TripService() {
    }

    public static TripService getInstance() {
        return INSTANCE;
    }
}
