package com.samarcev.servlet;

import com.samarcev.service.implementation.UserService;
import com.samarcev.util.RequestParam;
import com.samarcev.util.UrlPath;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet(UrlPath.DELETE_USER)
public class DeleteUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = (String) req.getSession().getAttribute(RequestParam.LOGIN);
        UserService.getInstance().deleteUser(login);
        log.info("delete user {}", login);
        resp.sendRedirect("login.jsp");
    }
}
