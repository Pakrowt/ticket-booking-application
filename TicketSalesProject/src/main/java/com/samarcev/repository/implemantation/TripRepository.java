package com.samarcev.repository.implemantation;

import com.samarcev.entity.Trip;
import com.samarcev.repository.TripRepositoryInterface;
import com.samarcev.util.HibernateSessionFactoryUtil;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;

import java.util.List;

@Slf4j
public class TripRepository implements TripRepositoryInterface {

    @Override
    public List<Trip> getAllTrips() {
        @Cleanup Session session = HibernateSessionFactoryUtil.getSession();
        session.beginTransaction();
        List<Trip> tripList = session.createQuery("from Trip ", Trip.class).setFirstResult(0).setMaxResults(10).getResultList();
        session.getTransaction().commit();
        return tripList;
    }
}
