package com.samarcev.repository;

import com.samarcev.entity.Role;
import com.samarcev.entity.User;

import java.util.List;

public interface UserRepositoryInterface {

    void createUser(String login, String password, String name, Role role, Double money);

    void deleteUser(String login);

    User getUser(String login);

    List<User> getAllUser();
}
