package com.samarcev.service.implementation;

import com.samarcev.entity.Bus;
import com.samarcev.repository.implemantation.BusRepository;
import com.samarcev.service.BusServiceInterface;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BusService implements BusServiceInterface {
    public static final BusService INSTANCE = new BusService();
    BusRepository busRepository = new BusRepository();

    @Override
    public Optional<Bus> getBusbyBusNumber(String busNumber) {
        return busRepository.getBusbyBusNumber(busNumber);
    }

    @Override
    public List<Bus> getAllBus() {
        return busRepository.getAllBus();
    }

    @Override
    public List<String> getBusNumbers() {
        return busRepository.getAllBus().stream().map(bus -> bus.getId()).collect(Collectors.toList());
    }

    private BusService() {
    }

    public static BusService getInstance() {
        return INSTANCE;
    }
}
